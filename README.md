OpenAPI to io-ts
===

**Warning**: This project is *experimental*, it supports only limited subset of OpenAPI.

Look at examples to see how it works and what is supported (open link in [Online section](#online) and click on the `%` next to the main caption).

Online
===
https://monnef.gitlab.io/openapi-to-io-ts

Installation
===
```sh
npm i
```

Development
===
```sh
npm start
```

Build
===
```sh
npm run build
```

Test
===
```sh
npm test
```
or interactive
```sh
npm run test:watch
```

License
===
AGPL3
