{Obj} = require 'prelude-ls'

export setClass = (name, el, value) -->
  if value
    then el.addClass name
    else el.removeClass name

export applyDefined = (f, x) --> if x then f x else x

export wrapStringIfNonEmpty = (wrap, x) --> if x != '' then wrap + x + wrap else x

export tap = (f, x) --> f x; x

export tapDebug = tap -> console.debug it

export stripVoidFields = Obj.filter -> it != void
