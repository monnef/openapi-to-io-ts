{partition, map, Str, filter, compact, id} = require 'prelude-ls'
{wrapStringIfNonEmpty, tapDebug} = require './utils.ls'

export genSchemaName = -> "#{it}Schema"

export fieldToIOTSType = (f) ->
  if f.ref then genSchemaName f.ref
  else if f.type
    switch f.type
      | \string   =>
        if f.enum then
          if f.enumSingle then "t.literal('#{f.enumSingle}')"
          else "#{f.name |> Str.capitalize}Schema"
        else 't.string'
      | \boolean  => 't.boolean'
      | \number   => 't.number'
      | \integer  => 't.Integer' # deprecated, but t.Int is branded = not of type number
      | \array    =>
        inner =  if f?.items?.type then fieldToIOTSType f.items
                 else if f?.items?.ref then genSchemaName f?.items?.ref
                 else ''
        "t.array(#{inner})"
      | _         => "null /* failed to convert type '#{f.type}' */"
  else "/* malformed input - no type nor ref fields are present */"

export fillIOTSType = (f) -> { ...f, iotsType: fieldToIOTSType(f) }

export toFieldStr = (opts, f) -->
  wrap = if opts.nullable && !f.required then -> "t.union([#{it}, t.null])" else id
  "  #{f.name}: #{wrap f.iotsType}"

export collectMultiEnums = (fs) -> fs |> filter (-> it.enum && !it.enumSingle)

export fieldToEnumSchema = (f) ->
  name = f.name |> Str.capitalize
  keys = f.enum |> map (-> "  #{it}: null") |> Str.join(',\n')
  """
export const #{name}Schema = t.keyof({
#{keys}
});

export type #{name} = t.TypeOf<typeof #{name}Schema>;
  """

importPart =
  """
  import * as t from 'io-ts';
  """

export genObjectOrAllOfTS = (opts, data) ->
  name = data.name
  extPart = if data.ext
    then "#{data.ext}Schema.type, "
    else ''
  processPartitioned = -> it |> map (toFieldStr opts) |> Str.join ',\n' |> wrapStringIfNonEmpty '\n'
  doPartition = partition (if opts.nullable then -> true else (.required))
  [requiredInnerPart, optionalInnerPart] = data.fields |> map fillIOTSType |> tapDebug |> doPartition |> map processPartitioned
  enumSchemaPart = data.fields |> collectMultiEnums |> map fieldToEnumSchema |>
    Str.join('\n\n') |> wrapStringIfNonEmpty '\n'
  constOptionalPart = if opts.nullable
    then null
    else "const optionalPart = t.partial({#{optionalInnerPart}});\n"
  constRequiredPart = "const requiredPart = t.interface({#{requiredInnerPart}});"
  constRequiredAndOptionalPart = [constOptionalPart, constRequiredPart] |> compact |> Str.join('\n')
  intersectionArrayItemsPart = "#{extPart}" + (if opts.nullable then "" else "optionalPart, ") + "requiredPart"
  insideExactPart = if opts.nullable && !data.ext then intersectionArrayItemsPart
                                                  else "t.intersection([#{intersectionArrayItemsPart}])"
  constSchemaPart = "export const #{name}Schema = t.exact(#{insideExactPart});"
  """
  #{importPart}
  #{enumSchemaPart || ''}
  #{constRequiredAndOptionalPart}

  #{constSchemaPart}

  export interface #{name} extends t.TypeOf<typeof #{name}Schema> {}
  """

export genOneOfTS = (data) ->
  name = data.name
  unionMembersPart = data.members |> map genSchemaName |> Str.join ', '
  schemaLine = if data.tagName
    then "// tag is field '#{data.tagName}'\nexport const #{name}Schema = t.union([#{unionMembersPart}]);"
    else "export const #{name}Schema = t.union([#{unionMembersPart}]);"
  """
  #{importPart}

  #{schemaLine}

  export type #{name} = t.TypeOf<typeof #{name}Schema>;
  """

export genRawEnum = (data) ->
  f = { name: data.name, enum: data.enumVals }
  enumPart = fieldToEnumSchema f
  """
  #{importPart}

  #{enumPart}
  """

export genTS201 = (opts, data) -->
  console.debug 'genTS201', opts, data
  switch data.schemaType
    | \object, \allOf => genObjectOrAllOfTS opts, data
    | \oneOf => genOneOfTS data
    | \enum => genRawEnum data
    | _ => void
