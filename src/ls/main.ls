require! jquery: $
require! yaml: YAML
require! brace: ace
{Str, map, pairs-to-obj, head} = require 'prelude-ls'
{debounce} = require 'lodash/fp'

require 'brace/mode/yaml'
require 'brace/mode/typescript'

{fillExamples} = require './examples.ls'
{process} = require './processData.ls'
{setClass, applyDefined} = require './utils.ls'
{genTS, ioTsVersions} = require './genTS.ls'

inputEdId = 'code-input'
outputEdId = 'code-output'

editorTheme = 'kr_theme'
require "brace/theme/#editorTheme"

codesId = \codes
debounceTime = 250

yamlToTS = (opts, version, data) --> data |> YAML.parse |> (|| {}) |> process opts |> genTS opts, version

parsePropMap = (str) -> str.split /, */ |> map Str.split '->' |> pairs-to-obj

updateOutput = ({getInputCode, setOutputCode, setErrorState, getSettings }) !->
  s = getSettings!
  opts =
    propMap: parsePropMap (s.propMap || '')
    schemaTranslation: s.schemaTranslation
    nullable: s.nullable
  try
    getInputCode! |> yamlToTS opts, s.version |> setOutputCode
    setErrorState off
  catch err
    stack = 'Stack trace not available.'
    try
      console.error err
      stack = err.stack |> JSON.stringify
    "\nERROR: #{err.message}\n\n#{stack}\n" |> setOutputCode
    setErrorState on

setupEditors = ->
  create = (id, type) ->
    ed = ace.edit id
    ed.$blockScrolling = Infinity
    ed.getSession!.setMode "ace/mode/#type"
    ed.setTheme "ace/theme/#editorTheme"
    ed
  edInput = create inputEdId, \yaml
  edOutput = create outputEdId, \typescript
  edOutput.setReadOnly on
  {edInput, edOutput}

setupSettings = (codeAccessor) ->
  ls = window.localStorage
  key = \settings
  horizLayoutEl = $ \#horizLayout
  propMapEl = $ \#propMap
  verEl = $ \#ioTsVersion
  schemaTranslationEl = $ \#schemaTranslation
  nullableEl = $ \#nullable
  setupVersions = ->
    ioTsVersions.forEach (v, i) ->
      opt = $(\<option>).attr(\value, v).text(v)
      verEl.append opt
      opt.attr('selected', true) if i == 0
  loadSettings = ->
    res = {}
    try
      res = ls.getItem key |> JSON.parse
    catch err
      console.error 'Loading settings failed.', err
    res || {}
  saveSettings = (s) !->
    ls.setItem key, JSON.stringify s
    console.log 'Settings saved', s
  settingsFromForm = ->
    horizLayout: horizLayoutEl.prop \checked
    propMap: propMapEl.val!
    version: verEl.val!
    schemaTranslation: schemaTranslationEl.prop \checked
    nullable: nullableEl.prop \checked
  onSettingsChange = !->
    s = settingsFromForm!
    saveSettings s
    codeAccessor.updateOutput!
    setClass \horizontal, ($ "\##codesId"), s.horizLayout
  loadSettingsToForm = !->
    s = loadSettings!
    horizLayoutEl.prop \checked, s.horizLayout
    propMapEl.val s.propMap
    verEl.val (s.version ? head ioTsVersions)
    schemaTranslationEl.prop \checked, s.schemaTranslation ? on
    nullableEl.prop \checked, s.nullable

  horizLayoutEl.change onSettingsChange
  propMapEl.change onSettingsChange
  verEl.change onSettingsChange
  schemaTranslationEl.change onSettingsChange
  nullableEl.change onSettingsChange

  setupVersions!
  loadSettingsToForm!
  onSettingsChange!
  { getSettings: loadSettings }

setupExamplesToggle = (toggleEl, examplesEl) !->
  console.log(toggleEl, examplesEl)
  toggleEl.click !-> examplesEl.toggle!

export main = !->
  console.info '%cOpenAPI to io-ts', 'font-size: 150%; color: purple'
  {edInput, edOutput} = setupEditors!
  getInputCode = -> edInput.getValue!
  setInputCode = !->
    edInput.setValue it + '\n'
    edInput.clearSelection!
  setOutputCode = !->
    edOutput.setValue it + '\n'
    edOutput.clearSelection!
  setErrorState = (x) !-> setClass \error, $("\##outputEdId").parent!, x
  codeAccessorForSettings = { updateOutput: !-> }
  {getSettings} = setupSettings codeAccessorForSettings
  updateOutputParamLess = !-> updateOutput {getInputCode, setOutputCode, setErrorState, getSettings}
  codeAccessorForSettings.updateOutput = updateOutputParamLess
  edInput.session.on \change, (debounce debounceTime, updateOutputParamLess)
  examplesEl = $ \.examples
  fillExamples { setInputCode }, examplesEl
  examplesWrapperEl = $ \.examples-wrapper
  setupExamplesToggle ($ \.examples-toggle), examplesWrapperEl
