require! jquery: $
{each} = require 'prelude-ls'

{examples} = require './examplesData.ls';

createExampleEl = (codeAccess, wrapper, ex) -->
  console.debug 'createExampleEl', ex
  h = !-> codeAccess.setInputCode ex.src
  el = $ '<a>' .text ex.name .attr 'href', 'javascript: void(0)' .click h
  wrapper.append el

export fillExamples = (codeAccess, container) !->
  console.debug 'fillExamples', codeAccess, container, examples
  examples |> each (x) !-> createExampleEl codeAccess, container, x
  container.children!.0.click!
