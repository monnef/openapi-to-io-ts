{partition, map, Str, filter} = require 'prelude-ls'
{wrapStringIfNonEmpty, tapDebug} = require './utils.ls'

export genSchemaName = -> "#{it}Schema"

export fieldToIOTSType = (f) ->
  if f.ref then genSchemaName f.ref
  else if f.type
    switch f.type
      | \string   =>
        if f.enum then
          if f.enumSingle then "t.literal('#{f.enumSingle}')"
          else "#{f.name |> Str.capitalize}Schema"
        else 't.string'
      | \boolean  => 't.boolean'
      | \number   => 't.number'
      | \integer  => 't.Integer'
      | \array    =>
        inner =  if f?.items?.type then fieldToIOTSType f.items
                 else if f?.items?.ref then genSchemaName f?.items?.ref
                 else ''
        "t.array(#{inner})"
      | _         => "null /* failed to convert type '#{f.type}' */"
  else "/* malformed input - no type nor ref fields are present */"

export fillIOTSType = (f) -> { ...f, iotsType: fieldToIOTSType(f) }

export toFieldStr = (f) -> "  #{f.name}: #{f.iotsType}"

export collectMultiEnums = (fs) -> fs |> filter (-> it.enum && !it.enumSingle)

export fieldToEnumSchema = (f) ->
  name = f.name |> Str.capitalize
  keys = f.enum |> map (-> "  #{it}: null") |> Str.join(',\n')
  """
export const #{name}Schema = t.keyof({
#{keys}
});

export type #{name} = t.TypeOf<typeof #{name}Schema>;
  """

importPart =
  """
  import * as t from 'io-ts';
  """

export genObjectOrAllOfTS = (data) ->
  name = data.name
  extPart = if data.ext
    then "#{data.ext}Schema.type, "
    else ''
  processPartitioned = -> it |> map toFieldStr |> Str.join ',\n' |> wrapStringIfNonEmpty '\n'
  [requiredPart, optionalPart] = data.fields |> map fillIOTSType |> tapDebug |>
    partition (.required) |> map processPartitioned
  enumSchemaPart = data.fields |> collectMultiEnums |> map fieldToEnumSchema |>
    Str.join('\n\n') |> wrapStringIfNonEmpty '\n'
  """
  #{importPart}
  #{enumSchemaPart || ''}
  const optionalPart = t.partial({#{optionalPart}});
  const requiredPart = t.interface({#{requiredPart}});

  export const #{name}Schema = t.exact(t.intersection([#{extPart}optionalPart, requiredPart]));

  export interface #{name} extends t.TypeOf<typeof #{name}Schema> {}
  """

export genOneOfTS = (data) ->
  name = data.name
  unionMembersPart = data.members |> map genSchemaName |> Str.join ', '
  schemaLine = if data.tagName
    then "export const #{name}Schema = t.taggedUnion('#{data.tagName}', [#{unionMembersPart}]);"
    else "export const #{name}Schema = t.union([#{unionMembersPart}]);"
  """
  #{importPart}

  #{schemaLine}

  export type #{name} = t.TypeOf<typeof #{name}Schema>;
  """

export genRawEnum = (data) ->
  f = { name: data.name, enum: data.enumVals }
  enumPart = fieldToEnumSchema f
  """
  #{importPart}

  #{enumPart}
  """

export genTS141 = (opts, data) -->
  console.debug 'genTS141', data
  switch data.schemaType
    | \object, \allOf => genObjectOrAllOfTS data
    | \oneOf => genOneOfTS data
    | \enum => genRawEnum data
    | _ => void
