{Str, last, Obj, map, apply} = require 'prelude-ls'

{applyDefined, stripVoidFields} = require './utils.ls'

export refToClassName = (x) -> if x
  then x.replace /\.yaml$/, '' |> /([^/.]+)$/.exec |> (.1) |> Str.capitalize
  else x

export applyPropMap = (propMap, x) --> propMap[x] || x

schemaTranslationDb = Object.freeze({
  Boolean: { type: \boolean }
  Double: { type: \number }
  Int32: { type: \integer }
  Int32Array: { type: \array, items: {type: \integer} }
  Int64: { type: \integer }
  Int64Array: { type: \array, items: {type: \integer} }
})

getRecordFromSchemaTranslation = (className) -> schemaTranslationDb[className]

export createField = (opts, rawName, x) -->
  name = applyPropMap opts.propMap, rawName
  if x.$ref then
    clsName = refToClassName x.$ref
    res =
      if opts.schemaTranslation && getRecordFromSchemaTranslation(clsName) then
        { name, ...getRecordFromSchemaTranslation(clsName) }
      else { name, ref: clsName }
    res |> stripVoidFields
  else
    items =
      if x.type == \array then createField opts, undefined, x.items
      else void
    enumVals = x.enum
    enumSingle = if enumVals && enumVals.length == 1 then enumVals.0
                 else void
    { name, type: x.type, items, enum: enumVals, enumSingle } |> stripVoidFields

export addRequired = (requiredList, field) -->
  { ...field, required: requiredList.includes field.name }

export processObject = (opts, obj) ->
  requiredList = obj.required || [] |> map applyPropMap opts.propMap
  fields = obj |> (.properties) |> Obj.obj-to-pairs |>
    map (apply (createField opts)) |> map (addRequired requiredList)
  fields

export getName = ({obj, data, schemaType}) ->
  r = switch schemaType
    | \object, \allOf => obj.title
    | \oneOf, \enum => data.title
    | _ => void
  r || \Name

getSchemaTypeAndObj = ({ data }) ->
  obj = data
  schemaType = \object
  if data.allOf then
    obj = data.allOf |> last
    schemaType = \allOf # TODO: support merging?
  if data.oneOf then
    obj = data.oneOf
    schemaType = \oneOf
  if data.enum then
    obj = data.enum
    schemaType = \enum
  { obj, schemaType }

getFields = ({ schemaType, obj, opts }) ->
  switch schemaType
    | \object, \allOf => processObject opts, obj
    | _ => void

getMembers =  ({ schemaType, obj }) ->
  if schemaType == \oneOf then obj |> map (.$ref) |> map refToClassName
  else void

getTagName = ({ schemaType, data, opts }) ->
  if schemaType == \oneOf then applyPropMap opts.propMap, data.discriminator?.propertyName
  else void

getEnumVals = ({ schemaType, obj }) ->
  if schemaType == \enum then obj
  else void

export process = (opts, data) -->
  # console.debug 'process', {opts, data}
  ext = refToClassName data.allOf?.0?.$ref
  { obj, schemaType } = getSchemaTypeAndObj { data }
  name = getName { obj, data, schemaType }
  fields = getFields { schemaType, obj, opts }
  members = getMembers { schemaType, obj }
  tagName = getTagName { schemaType, data, opts }
  enumVals = getEnumVals { schemaType, obj }
  {
    name
    ext
    fields
    schemaType
    members
    tagName
    enumVals
  } |> stripVoidFields
