{map, zip} = require 'prelude-ls'

exampleNames =
  * 'inheritance + required'
  * 'field $ref'
  * 'array'
  * 'union'
  * 'tagged union'
  * '1 value enum'
  * 'enum'
  * 'raw enum'

export examples =
  [1 to exampleNames.length]
  |> map -> require("text-loader!./examplesData/example#{if it < 10 then '0' else ''}#{it}.yaml")
  |> zip exampleNames
  |> map -> { src: it.1, name: it.0 }
