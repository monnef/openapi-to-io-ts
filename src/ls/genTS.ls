{} = require 'prelude-ls'

{genTS141} = require './genTS141.ls'
{genTS201} = require './genTS201.ls'

v141 = "1.4.1"
v201 = "2.0.1"

export ioTsVersions = [v201, v141]

export genTS = (opts, version, data) -->
  console.debug 'genTS', opts, version, data
  p = switch version
    | v141 => genTS141
    | v201 => genTS201
    | _ => -> "unsupported version #{version}"
  p opts, data
