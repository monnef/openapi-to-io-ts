should = require('chai').should!

require! '../src/ls/genTS201.ls': {
  genSchemaName, fieldToIOTSType, fillIOTSType, toFieldStr, genTS
}

genF1 = ->
  type: \string
  name: \f1

genF2 = ->
  type: \boolean
  name: \f2
  required: true

genF3 = ->
  type: \number
  name: \f3

genF4 = ->
  type: \integer
  name: \f4

genF5 = ->
  type: \array
  items:
    type: \string
    note: 'f5 array item'
  name: \f5

genF6 = ->
  type: \array
  items:
    ref: \Orville
    note: 'f6 array ref item'
  name: \f6

genF7 = ->
  ref: \Voyager
  name: \f7

genF8 = ->
  type: \string
  enum: [\UNIT]
  enumSingle: \UNIT
  name: \f8

genF9 = ->
  type: \string
  enum: [\MALE \FEMALE]
  name: \f9

genOptsUndef = -> { -nullable }

genOptsNullable = -> { +nullable }

describe 'genTS201' ->
  specify 'genSchemaName' ->
    genSchemaName 'x' .should.equal 'xSchema'
    genSchemaName 'Email' .should.equal 'EmailSchema'

  describe 'fieldToIOTSType' ->
    specify 'primitives' ->
      fieldToIOTSType genF1! .should.equal 't.string'
      fieldToIOTSType genF2! .should.equal 't.boolean'
      fieldToIOTSType genF3! .should.equal 't.number'
      fieldToIOTSType genF4! .should.equal 't.Integer'
    specify 'array' ->
      fieldToIOTSType genF5! .should.equal 't.array(t.string)', 'array'
      fieldToIOTSType genF6! .should.equal 't.array(OrvilleSchema)', 'array'
    specify 'ref' ->
      fieldToIOTSType genF7! .should.equal 'VoyagerSchema'
    specify 'enum' ->
      fieldToIOTSType genF8! .should.equal 't.literal(\'UNIT\')'
      fieldToIOTSType genF9! .should.equal 'F9Schema'
    specify 'bad input' ->
      malformedMsg = 'malformed input'
      failMsg = 'failed to convert'
      fieldToIOTSType {} .should.include malformedMsg, 'missing'
      fieldToIOTSType {type: null} .should.include malformedMsg, 'null'
      fieldToIOTSType {type: 'Spaceship'} .should.include failMsg, 'unknown'
      fieldToIOTSType {type: 'Galactica'} .should.include failMsg, 'unknown2'

  specify 'fillIOTSType' ->
    fillIOTSType genF1! .should.have.property 'iotsType', 't.string'
    fillIOTSType genF7! .should.have.property 'iotsType', 'VoyagerSchema'

  specify 'toFieldStr' ->
    fU = fillIOTSType >> toFieldStr genOptsUndef!
    fN = fillIOTSType >> toFieldStr genOptsNullable!
    fU genF1! .should.equal '  f1: t.string'
    fU genF5! .should.equal '  f5: t.array(t.string)'
    fU genF7! .should.equal '  f7: VoyagerSchema'
    fN genF1! .should.equal '  f1: t.union([t.string, t.null])'
    fN genF2! .should.equal '  f2: t.boolean'
