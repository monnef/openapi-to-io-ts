should = require('chai').should!

require! '../src/ls/processData.ls': {
  refToClassName
  createField
  addRequired
  process
}

genEmptyOpts = -> {propMap:{}}

describe 'processData' ->
  describe 'refToClassName' ->
    specify 'outer' ->
      refToClassName 'UpdateItem.yaml' .should.equal 'UpdateItem'
    specify 'outer complex' ->
      refToClassName '../../openapi.yaml#/components/schemas/Email' .should.equal 'Email'
    specify 'outer lower case' ->
      refToClassName 'updateItem.yaml' .should.equal 'UpdateItem'
    specify 'inner' ->
      refToClassName '#/components/schemas/user' .should.equal 'User'

  describe 'createField' ->
    specify 'passes values' ->
      createField genEmptyOpts!, \a, { type: \T } .should.eql { name: \a, type: \T }
    specify 'translates ref' ->
      createField genEmptyOpts!, \a, { $ref: 'T.yaml' } .should.eql { name: \a, ref: \T }
    specify 'arrays' ->
      createField genEmptyOpts!, \a, { type: \array, items: { type: \string } }
      .should.eql { name: \a, type: \array, items: { type: \string } }
      createField genEmptyOpts!, \a, { type: \array, items: { $ref: '../I.yaml' } }
      .should.eql { name: \a, type: \array, items: { ref: \I } }
    specify 'enum' ->
      createField genEmptyOpts!, \a, { type: \string, enum: [\A \B] }
      .should.eql { name: \a, type: \string, enum: [\A \B] }
      createField genEmptyOpts!, \a, { type: \string, enum: [\A] }
      .should.eql { name: \a, type: \string, enum: [\A], enumSingle: \A }
    specify 'propMap' ->
      createField {propMap: {a: \b}}, \a, { type: \T } .should.eql { name: \b, type: \T }
    describe 'translates schema references to primitive/specialized types' ->
      specify 'boolean' ->
        createField {...genEmptyOpts!, schemaTranslation: on}, \a,
          { $ref: '../../openapi.yaml#/components/schemas/Boolean' }
        .should.eql { name: \a, type: \boolean }

      specify 'double' ->
        createField {...genEmptyOpts!, schemaTranslation: on}, \a,
          { $ref: '../../openapi.yaml#/components/schemas/Double' }
        .should.eql { name: \a, type: \number }

      specify 'int32' ->
        createField {...genEmptyOpts!, schemaTranslation: on}, \a,
          { $ref: '../../openapi.yaml#/components/schemas/Int32' }
        .should.eql { name: \a, type: \integer }

      specify 'int64' ->
        createField {...genEmptyOpts!, schemaTranslation: on}, \a,
          { $ref: '../../openapi.yaml#/components/schemas/Int64' }
        .should.eql { name: \a, type: \integer }

      specify 'int32array' ->
        createField {...genEmptyOpts!, schemaTranslation: on}, \a,
          { $ref: '../../../openapi.yaml#/components/schemas/Int32Array' }
        .should.eql { name: \a, type: \array, items: { type: \integer } }

      specify 'int64array' ->
        createField {...genEmptyOpts!, schemaTranslation: on}, \a,
          { $ref: '../../../openapi.yaml#/components/schemas/Int64Array' }
        .should.eql { name: \a, type: \array, items: { type: \integer } }

      specify 'nested int64array' ->
        createField {...genEmptyOpts!, schemaTranslation: on}, \a,
          { type: \array, items: {$ref: '../../../openapi.yaml#/components/schemas/Int64Array'} }
        .should.eql { name: \a, type: \array, items: { type: \array, items: { type: \integer } } }

  describe 'addRequired' ->
    specify \pos ->
      addRequired [\a \b] { name: \a, type: \string } .should.eql { name: \a, type: \string, required: true }
    specify \neg ->
      addRequired [\a \b] { name: \c, type: \string } .should.eql { name: \c, type: \string, required: false }

  describe 'process' ->
    specify 'name' ->
      obj = { title: \A, properties: [] }
      (process genEmptyOpts!, obj).name .should.equal \A
    specify 'properties' ->
      obj = { title: \A, properties: { code: { type: \integer} } }
      opts = {propMap: {code: \cipher}}
      process opts, obj .should.eql { name: \A, schemaType: \object, fields: [{ name: \cipher, type: \integer, required: false }] }
    specify 'required with propMap' ->
      obj = { title: \A, properties: { code: { type: \integer} }, required: [\code, \x] }
      opts = {propMap: {code: \cipher}}
      process opts, obj .should.eql { name: \A, schemaType: \object, fields: [{ name: \cipher, type: \integer, required: true }] }
    describe 'allOf' ->
      specify 'fills extend field' ->
        obj = { allOf: [{ $ref: '../B.yaml' }, { title: \A, properties: [] }] }
        process genEmptyOpts!, obj .ext .should.eql \B
      specify 'passes props' ->
        properties =
          a:
            type: \string
          b:
            $ref: '../B.yaml'
        obj = { allOf: [{ $ref: '../B.yaml' }, { title: \A, properties, required: [\b] }] }
        exp =
          * name: \a
            required: no
            type: \string
          * name: \b
            required: yes
            ref: \B
        process genEmptyOpts!, obj .fields .should.eql exp
    describe 'oneOf' ->
      specify 'schemaType' ->
        process genEmptyOpts!, { oneOf: [], title: \A } .should.eql { schemaType: \oneOf, name: \A, members: [] }
      specify 'members' ->
        process genEmptyOpts!, { oneOf: [{ $ref: 'A.yaml' }, { $ref: '../B.yaml' }], title: \A }
        .should.eql { schemaType: \oneOf, name: \A, members: [\A \B] }
      specify 'tagName' ->
        obj = { oneOf: [{ $ref: 'A.yaml' }, { $ref: '../B.yaml' }], title: \A, discriminator: { propertyName: \x } }
        process genEmptyOpts!, obj .should.eql { schemaType: \oneOf, name: \A, members: [\A \B], tagName: \x }
      specify 'tagName + propMap' ->
        obj = { oneOf: [{ $ref: 'A.yaml' }, { $ref: '../B.yaml' }], title: \A, discriminator: { propertyName: \x } }
        opts = {propMap: {x: \y}}
        process opts, obj .should.eql { schemaType: \oneOf, name: \A, members: [\A \B], tagName: \y }
    describe 'enum' ->
      specify 'enumVals' ->
        process genEmptyOpts!, { enum: [\A \B], title: \E }
        .should.eql { schemaType: \enum, name: \E, enumVals: [\A \B] }
