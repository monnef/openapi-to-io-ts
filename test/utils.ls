require! 'chai':{assert, expect}
chai = require('chai')
spies = require('chai-spies')
chai.use spies
chai.should!

require! '../src/ls/utils.ls':{wrapStringIfNonEmpty, applyDefined, tap, tapDebug, stripVoidFields}

sandbox = chai.spy.sandbox!
origConsoleDebug = console.debug

describe 'utils' ->
  beforeEach ->
    console.debug = chai.spy (r) -> 20

  afterEach ->
    console.debug = origConsoleDebug

  specify 'wrapStringIfNonEmpty' ->
    wrapStringIfNonEmpty 'x', '' .should.equal ''
    wrapStringIfNonEmpty 'x', 'y' .should.equal 'xyx'

  specify 'applyDefined' ->
    crash = -> throw ''
    applyDefined crash, void
    applyDefined (+ 1), 2 .should.equal 3

  specify 'tap' ->
    f = chai.spy (r) -> 2
    tap f, 1 .should.equal 1
    f.should.have.been.called.once.with 1

  specify 'tapDebug' ->
    tapDebug 1 .should.equal 1
    console.debug.should.have.been.called.once.with 1

  specify 'stripVoidFields' ->
    stripVoidFields {a: void, b: void, c: 4, d: \d} .should.eql {c: 4, d: \d}
